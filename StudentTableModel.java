import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Dinz
 */
public class StudentTableModel {
    private ArrayList<Student> student;
    private DefaultTableModel tableModel;
    private String search;

    public StudentTableModel(ArrayList<Student> student, String search) {
        this.student = student;
        this.search = search;
    }

    private void buildTable() {

        tableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        
        tableModel.addColumn("Index");
        tableModel.addColumn("NIS");
        tableModel.addColumn("Nama");
        tableModel.addColumn("Nilai Pretest");
        tableModel.addColumn("Nilai Posttest");
        tableModel.addColumn("Nilai Akhir");
        tableModel.addColumn("Predikat");

        float finalScore;
        String grade;

        for(int i=0; i<student.size(); i++) {
            if(!search.equals("")){
                if(!student.get(i).getName().toLowerCase().contains(search.toLowerCase())){
                    continue;
                }
            }
            
            finalScore = (student.get(i).getPretestScore() + student.get(i).getPosttestScore()) / 2;
            grade = getGrade(finalScore);
            tableModel.addRow(
                    new Object[]{
                        i,
                        student.get(i).getId(),
                        student.get(i).getName(),
                        student.get(i).getPretestScore(),
                        student.get(i).getPosttestScore(),
                        finalScore,
                        grade
                    }
            );
        }
    }

    public String getGrade(float score) {
        String grade = "-";
        if (score > 80) {
            grade = "A";
        } else if (score > 70) {
            grade = "B";
        } else if (score > 60) {
            grade = "C";
        } else if (score > 50) {
            grade = "D";
        } else if (score > 10) {
            grade = "E";
        }

        return grade;
    }

    public DefaultTableModel getTableModel() {
        buildTable();
        return tableModel;
    }
}